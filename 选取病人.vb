﻿Imports System.Data
Imports System.Data.SqlClient
Public Class 选取病人
    Dim 病人列举命令 As SqlCommand
    Dim 病人列举读取 As System.Data.SqlClient.SqlDataReader
    Dim 选取的身份证号() As String
    Public 选取模式 As Int16 = 0 '选取模式，1为可入院，2为仅住院，3为住院和今日门诊
    Public 更新需求 As Boolean
    Dim micedown As Boolean = False
    Private Sub 选取病人_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        TextBox2.Text = ""
        TextBox1.Text = ""
        Timer1.Stop()
        Timer2.Stop()
        触发查询()
        Select Case 选取模式
            Case 0, 1
                LinkLabel1.Visible = True
            Case Else
                LinkLabel1.Visible = False
        End Select
    End Sub
    Private Sub 触发查询()
        Timer1.Stop()
        ReDim 选取的身份证号(1000)
        Dim 读取计数 = 0
        Dim 身份证号缓存 As String
        Select Case 选取模式
            Case 1
                病人列举命令 = New SqlCommand(
                    "获取可入院门诊病人信息 @姓名='" & TextBox1.Text & "',@身份证号码='" & TextBox2.Text & "'")
            Case 2
                病人列举命令 = New SqlCommand(
                    "获取在院病人信息 @姓名='" & TextBox1.Text & "',@身份证号码='" & TextBox2.Text & "'")
            Case 3 '可治疗病人
                病人列举命令 = New SqlCommand(
                    "获取可治疗病人信息 @姓名='" & TextBox1.Text & "',@身份证号码='" & TextBox2.Text & "'")
            Case Else
                病人列举命令 = New SqlCommand(
                    "获取全局病人信息 @姓名='" & TextBox1.Text & "',@身份证号码='" & TextBox2.Text & "'")
                '              "SELECT /*top (1000)*/
                ' [姓名]
                '    ,[身份证号码]
                '    ,[隐私化身份证号码]
                '    ,[性别]
                '    ,[年龄]
                '    ，'' 现就诊科室
                '    --,[出生年月]
                '    ,[联系方式]
                '    ,[病人备注信息]
                'FROM 在院病人信息
                'where [姓名] like '%" & TextBox1.Text & "%' and [身份证号码] like '%" & TextBox2.Text & "%'")
        End Select

        病人列举命令.Connection = 登录.连接
        ListView1.Items.Clear()
        登录.连接.Open()
        病人列举读取 = 病人列举命令.ExecuteReader
        Do While 病人列举读取.Read()
            ListView1.Items.Add(读取计数)
            ListView1.Items(读取计数).SubItems.Add(病人列举读取.GetString(0))
            选取的身份证号(读取计数) = 病人列举读取.GetString(1)
            身份证号缓存 = 病人列举读取.GetString(2)
            If 身份证号缓存 = "******************" Then
                身份证号缓存 = " （您不需要了解此字段）"
            End If
            ListView1.Items(读取计数).SubItems.Add(身份证号缓存)
            身份证号缓存 = ""
            ListView1.Items(读取计数).SubItems.Add(病人列举读取.GetValue(3))
            ListView1.Items(读取计数).SubItems.Add(病人列举读取.GetValue(4))
            ListView1.Items(读取计数).SubItems.Add(账单.getdataa(病人列举读取, 5))
            ListView1.Items(读取计数).SubItems.Add(账单.getdataa(病人列举读取, 6))
            ListView1.Items(读取计数).SubItems.Add(账单.getdataa(病人列举读取, 7))
            读取计数 = 读取计数 + 1
        Loop
        登录.连接.Close()
        更新需求 = False
    End Sub
    Private Sub Listview_doubleclick(sender As Object, e As EventArgs) Handles ListView1.DoubleClick
        If (ListView1.SelectedItems.Count = 0) Then
        Else
            micedown = False
            Me.Dispose()
        End If
    End Sub
    Private Sub Listview_mousedown(sender As Object, e As EventArgs) Handles ListView1.MouseDown
        'MsgBox("i am moving")
        micedown = True
        Timer2.Start()
    End Sub
    Private Sub Listview_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListView1.SelectedIndexChanged
        'MsgBox("一顿操作猛如虎")
        If (ListView1.SelectedItems.Count = 0) Then
        Else
            'MsgBox("nb!")
            主菜单.选中病人身份证号 = 选取的身份证号(Val(ListView1.SelectedItems.Item(0).Text))
        End If
    End Sub
    Private Sub Listview_mouseleave(sender As Object, e As EventArgs) Handles ListView1.MouseLeave
        Timer2.Stop()
        If micedown And ListView1.SelectedItems.Count > 0 Then
            If MsgBox("您确认吗将此人的信息从数据库中删除吗？", MsgBoxStyle.YesNo, "转科信息确认") = MsgBoxResult.Yes Then
                Dim 病人删除命令 As SqlCommand
                病人删除命令 = New SqlCommand(
            "delete from 病人信息表
            where 病人身份证号='" & 主菜单.选中病人身份证号 & "'")
                病人删除命令.Connection = 登录.连接
                登录.连接.Open()
                If 病人删除命令.ExecuteNonQuery() > 0 Then
                    MsgBox("成功！")
                End If
                登录.连接.Close()
                触发查询()
            End If
        End If
        micedown = False
    End Sub
    Private Sub Listview_mouseup(sender As Object, e As EventArgs) Handles ListView1.MouseUp
        micedown = False
        Timer2.Stop()
    End Sub
    Private Sub Timer2_Tick(sender As Object, e As EventArgs) Handles Timer2.Tick
        Timer2.Stop()
        micedown = False
        If (ListView1.SelectedItems.Count = 0) Then
        Else
            If 主菜单.选中病人身份证号 = 选取的身份证号(Val(ListView1.SelectedItems.Item(0).Text)) Then
                病人信息编辑.更新需求 = True
                病人信息编辑.Show()
                Me.Dispose()
                'MsgBox("成功触发")
            End If
        End If
    End Sub
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        触发查询()
        'MsgBox("i am moving")
    End Sub
    Private Sub TextBox2_TextChanged(sender As Object, e As EventArgs) Handles TextBox2.TextChanged
        Timer1.Stop()
        'MsgBox("2")
        Timer1.Start()
    End Sub
    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged
        Timer1.Stop()
        'MsgBox("1")
        Timer1.Start()
    End Sub
    Private Sub 窗体对焦(sender As Object, e As EventArgs) Handles Me.MouseDown, TextBox1.MouseDown, TextBox2.MouseDown, Label1.MouseDown, Label2.MouseDown, Label3.MouseDown, Label4.MouseDown, ListView1.MouseDown
        'MsgBox("一顿操作猛如虎")
        Select Case 选取模式
            Case 0, 1
                LinkLabel1.Visible = True
            Case Else
                LinkLabel1.Visible = False
        End Select
        If 更新需求 Then
            触发查询()
        End If
    End Sub
    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        病人信息管理.Show()
        'Me.Dispose()
    End Sub
End Class
