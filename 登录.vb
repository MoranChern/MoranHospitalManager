﻿Imports System.Data
Imports System.Data.SqlClient
Public Class 登录
    Public 连接 As SqlConnection = New SqlConnection
    Dim 账号 As String
    Dim 密码 As String
    Dim 输入状态 As Integer '1-》要输入密码，检查账号，账号正确变成2，2-》检查账号密码 不匹配变成1
    Dim 尝试次数 As Integer '1--账号错误 2--账号密码不匹配
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        连接.ConnectionString = "server=(local);database=程氏医院管理系统数据库;Integrated Security=True"
        账号 = ""
        密码 = ""
        输入状态 = 1
        尝试次数 = 0
        输入模式()
        Button2.Enabled = False
    End Sub
    Private Sub 输入模式()
        If 输入状态 = 1 Then
            Label2.Text = "请输入账号"
            Button2.Text = "下一步"
            Button1.Enabled = False
            Button1.Hide()
            TextBox1.Text = 账号
            TextBox1.PasswordChar = ""
            Label4.Text = "我们将检查您的账号是否可用"
        End If
        If 输入状态 = 2 Then
            Label2.Text = "请输入密码"
            Button2.Text = "登录"
            Button1.Enabled = True
            Button1.Show()
            TextBox1.Text = 密码
            TextBox1.PasswordChar = "+"
            Label4.Text = "欢迎你，" & 账号 & "！"
        End If
        TextBox1.SelectionStart = 0
        TextBox1.SelectionLength = Len(TextBox1.Text)
        TextBox1.Focus()
        Label3.Hide()
    End Sub
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Select Case 输入状态
            Case 1
                '账号 = TextBox1.Text
                If 账号 = "Admin" Then               ''''''''注意添加更多账号
                    输入状态 = 2
                    Call 输入模式()
                Else
                    Call 输入模式()
                    Label3.Text = "  账号不存在！  "
                    Label3.Show()
                End If
            Case 2
                If 账号 = "Admin" And 密码 = "123" Then              ''''''''注意添加更多账号和密码
                    主菜单.Show()
                    Me.Finalize()
                Else
                    输入状态 = 1
                    Call 输入模式()
                    Label3.Text = "账号密码不匹配！"
                    Label3.Show()
                    尝试次数 = 尝试次数 + 1
                    If 尝试次数 >= 5 Then
                        TextBox1.Enabled = False
                        TextBox1.Text = ""
                        MsgBox("已失败登录至少5次，程序已锁定，请重新启动程序")
                    End If
                End If
        End Select
    End Sub
    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged
        If TextBox1.Text = "" Then
            Button2.Enabled = False
        Else
            Button2.Enabled = True
        End If
        If 输入状态 = 1 Then
            账号 = TextBox1.Text
        End If
        If 输入状态 = 2 Then
            密码 = TextBox1.Text
        End If
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If 输入状态 = 2 Then
            输入状态 = 1
            输入模式()
        End If
    End Sub
End Class
