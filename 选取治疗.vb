﻿Imports System.Data
Imports System.Data.SqlClient
Public Class 选取治疗
    Dim 治疗列举命令 As SqlCommand
    Dim 治疗列举读取 As System.Data.SqlClient.SqlDataReader
    Public 选取模式 As Int16 = 0 '选取模式
    Public 更新需求 As Boolean
    Dim micedown As Boolean
    Private Sub 触发查询()
        Timer1.Stop()
        Dim 读取计数 = 0
        Select Case 选取模式
            Case 0
                治疗列举命令 = New SqlCommand("
                    /****** SSMS 的 SelectTopNRows 命令的脚本  ******/
                    Select /*Top (1000)*/  
                     [治疗项目编号]
                    ,dbo.治疗项目分类(治疗项目编号) 分类
                    ,[治疗项目名称]
                    ,[项目治疗费用]
                  --,[是否可叠加]
                    ,[治疗项目备注与注意事项]
                     From [程氏医院管理系统数据库].[dbo].[治疗项目说明表]
                     Where Left(治疗项目编号,2)!='ZY' 
                       and  治疗项目编号 like '%" & TextBox1.Text & "%'
                       and  治疗项目名称 like '%" & TextBox2.Text & "%'
                       and  项目治疗费用 between $" & TextBox3.Text & " and $" & TextBox4.Text & " ")
            Case 1
                治疗列举命令 = New SqlCommand("
                    /****** SSMS 的 SelectTopNRows 命令的脚本  ******/
                    Select /*Top (1000)*/  
                     [治疗项目编号]
                    ,dbo.治疗项目分类(治疗项目编号) 分类
                    ,[治疗项目名称]
                    ,[项目治疗费用]
                  --,[是否可叠加]
                    ,[治疗项目备注与注意事项]
                     From [程氏医院管理系统数据库].[dbo].[治疗项目说明表]
                     Where Left(治疗项目编号,2) = 'CZ'
                       and  治疗项目编号 like '%" & TextBox1.Text & "%'
                       and  治疗项目名称 like '%" & TextBox2.Text & "%'
                       and  项目治疗费用 between $" & TextBox3.Text & " and $" & TextBox4.Text & " ")
            Case 2 '可治疗病人
                治疗列举命令 = New SqlCommand("
                    /****** SSMS 的 SelectTopNRows 命令的脚本  ******/
                    Select /*Top (1000)*/  
                     [治疗项目编号]
                    ,dbo.治疗项目分类(治疗项目编号) 分类
                    ,[治疗项目名称]
                    ,[项目治疗费用]
                  --,[是否可叠加]
                    ,[治疗项目备注与注意事项]
                     From [程氏医院管理系统数据库].[dbo].[治疗项目说明表]
                     Where Left(治疗项目编号,2) = 'YP'
                       and  治疗项目编号 like '%" & TextBox1.Text & "%'
                       and  治疗项目名称 like '%" & TextBox2.Text & "%'
                       and  项目治疗费用 between $" & TextBox3.Text & " and $" & TextBox4.Text & " ")
            Case Else
                治疗列举命令 = New SqlCommand("
                    /****** SSMS 的 SelectTopNRows 命令的脚本  ******/
                    Select /*Top (1000)*/ 
                     [治疗项目编号]
                    ,dbo.治疗项目分类(治疗项目编号) 分类
                    ,[治疗项目名称]
                    ,[项目治疗费用]
                  --,[是否可叠加]
                    ,[治疗项目备注与注意事项]
                     From [程氏医院管理系统数据库].[dbo].[治疗项目说明表]
                     Where  治疗项目编号 like '%" & TextBox1.Text & "%'
                       and  治疗项目名称 like '%" & TextBox2.Text & "%'
                       and  项目治疗费用 between $" & TextBox3.Text & " and $" & TextBox4.Text & " ")
        End Select

        治疗列举命令.Connection = 登录.连接
        ListView1.Items.Clear()
        登录.连接.Open()
        治疗列举读取 = 治疗列举命令.ExecuteReader
        Do While 治疗列举读取.Read()
            ListView1.Items.Add(账单.getdataa(治疗列举读取, 0))
            ListView1.Items(读取计数).SubItems.Add(账单.getdataa(治疗列举读取, 1))
            ListView1.Items(读取计数).SubItems.Add(账单.getdataa(治疗列举读取, 2))
            ListView1.Items(读取计数).SubItems.Add(账单.getdataa(治疗列举读取, 3))
            ListView1.Items(读取计数).SubItems.Add(账单.getdataa(治疗列举读取, 4))
            读取计数 = 读取计数 + 1
        Loop
        登录.连接.Close()
        更新需求 = False
    End Sub
    Private Sub Listview_doubleclick(sender As Object, e As EventArgs) Handles ListView1.DoubleClick
        If (ListView1.SelectedItems.Count = 0) Then
        Else
            micedown = False
            Me.Dispose()
        End If
    End Sub
    Private Sub 选取治疗_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        TextBox2.Text = ""
        TextBox1.Text = ""
        TextBox3.Text = "0.00"
        TextBox4.Text = "1000.00"
        Timer1.Stop()
        Timer2.Stop()
        触发查询()
        Select Case 选取模式
            Case -1
                LinkLabel1.Visible = True
            Case Else
                LinkLabel1.Visible = False
        End Select
    End Sub
    Private Sub 窗体对焦(sender As Object, e As EventArgs) Handles Me.MouseDown, TextBox1.MouseDown, TextBox2.MouseDown, TextBox3.MouseDown, TextBox4.MouseDown, ComboBox1.MouseDown, Label1.MouseDown, Label2.MouseDown, Label3.MouseDown, Label4.MouseDown, ListView1.MouseDown
        'MsgBox("一顿操作猛如虎")
        Select Case 选取模式
            Case -1
                LinkLabel1.Visible = True
            Case Else
                LinkLabel1.Visible = False
        End Select
        If 更新需求 Then
            触发查询()
        End If
    End Sub
    Private Sub Listview_mousedown(sender As Object, e As EventArgs) Handles ListView1.MouseDown
        Timer2.Start()
        micedown = True
    End Sub
    Private Sub Listview_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListView1.SelectedIndexChanged
        If (ListView1.SelectedItems.Count = 0) Then
        Else
            'MsgBox("正常")
            创建医嘱.选定治疗编号 = ListView1.SelectedItems.Item(0).Text
        End If
    End Sub
    Private Sub Listview_mouseleave(sender As Object, e As EventArgs) Handles ListView1.MouseLeave
        Timer2.Stop()

        If micedown Then
            MsgBox("确认删除")
        End If
    End Sub
    Private Sub Listview_mouseup(sender As Object, e As EventArgs) Handles ListView1.MouseUp
        Timer2.Stop()
        micedown = False
    End Sub
    Private Sub Timer2_Tick(sender As Object, e As EventArgs) Handles Timer2.Tick
        Timer2.Stop()
        micedown = False
        If (ListView1.SelectedItems.Count = 0) Then
        Else
            If 创建医嘱.选定治疗编号 = ListView1.SelectedItems.Item(0).Text Then
                治疗信息编辑.更新需求 = True
                治疗信息编辑.Show()
                Me.Dispose()
            End If
        End If
    End Sub
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        触发查询()
    End Sub
    Private Sub _TextChanged(sender As Object, e As EventArgs) Handles TextBox2.TextChanged, TextBox1.TextChanged, TextBox3.TextChanged, TextBox4.TextChanged, ComboBox1.SelectedIndexChanged
        Timer1.Stop()
        Timer1.Start()
    End Sub
    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        治疗信息添加.Show()
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged
        选取模式 = ComboBox1.SelectedIndex
    End Sub
End Class