﻿Imports System.Data
Imports System.Data.SqlClient
Public Class 账单
    Dim 身份证号缓存 As String
    Dim 选取模式 As Int16 = 1 '0为都选，1为未付款，2为已付款
    Dim 当前总价 As Decimal
    Public Function getdataa(ByVal dr As System.Data.SqlClient.SqlDataReader, ByVal ord As Int16)
        If dr.IsDBNull(ord) Then
            Return " "
        Else
            Return dr.GetValue(ord)
        End If
    End Function
    Private Sub 身份证号刷新()
        Label7.Text = "（无）"
        Label8.Text = "（无）"
        Label9.Text = "（无）"
        Label10.Text = "（无）"
        Label12.Text = "（无）"
        Label14.Text = "（无）"
        TextBox1.Text = "（无）"
        TextBox2.Text = "（无）"
        Dim 病人选取命令 As SqlCommand = New SqlCommand(
"SELECT /*top (1000)*/
	  [姓名]
	  ,'在院' 状态
      ,[隐私化身份证号码]
      ,[性别]
      ,[年龄]
	  ,科室名称 现就诊科室
    --,[出生年月]
      ,[联系方式]
      ,[病人备注信息]
  FROM 在院病人信息
  where 身份证号码='" & 主菜单.选中病人身份证号 & "'
  Union All
  SELECT /*top (1000)*/
	  [姓名]
      ,'今日门诊' 状态
      ,[隐私化身份证号码]
      ,[性别]
      ,[年龄]
	  ,科室名称 现就诊科室
    --,[出生年月]
      ,[联系方式]
      ,[病人备注信息]
  FROM 今日门诊
  where 身份证号码='" & 主菜单.选中病人身份证号 & "'
  Union all
  SELECT /*top (1000)*/
	  [姓名]
      ,'不在院' 状态
      ,[隐私化身份证号码]
      ,[性别]
      ,[年龄]
	  ,'（无）' 现就诊科室
    --,[出生年月]
      ,[联系方式]
      ,[病人备注信息]
  FROM 病人信息
  where 身份证号码='" & 主菜单.选中病人身份证号 & "' and 身份证号码 not in 
  (select 身份证号码 from 今日门诊 Union all select 身份证号码 from 在院病人信息)")
        Dim 选中病人信息读取 As System.Data.SqlClient.SqlDataReader '此行参考自学长的课题设计
        病人选取命令.Connection = 登录.连接
        登录.连接.Open()
        'MsgBox("正在刷新")
        选中病人信息读取 = 病人选取命令.ExecuteReader
        Do While 选中病人信息读取.Read()
            Label7.Text = 选中病人信息读取.GetString(0)
            Label14.Text = 选中病人信息读取.GetString(1)
            Label8.Text = 选中病人信息读取.GetValue(2)
            Label10.Text = 选中病人信息读取.GetString(3)
            Label9.Text = 选中病人信息读取.GetValue(4)
            Label12.Text = 选中病人信息读取.GetString(5)
            TextBox1.Text = 选中病人信息读取.GetString(6)
            TextBox2.Text = 选中病人信息读取.GetString(7)
        Loop
        登录.连接.Close()
        If Label8.Text = "******************" Then
            Label8.Text = "（您不需要了解此字段）"
        End If
        '------------------------------------------------
        Dim 读取计数 As Integer = 0
        Dim 账单命令 As SqlCommand = New SqlCommand(
          "EXECUTE 列举账单 
          @身份证号码='" & 主菜单.选中病人身份证号 & "'
         ,@选取模式=" & 选取模式)
        Dim 账单读取 As System.Data.SqlClient.SqlDataReader
        账单命令.Connection = 登录.连接
        登录.连接.Open()
        账单读取 = 账单命令.ExecuteReader
        ListView1.Items.Clear()
        当前总价 = 0
        Do While 账单读取.Read()
            ListView1.Items.Add(getdataa(账单读取, 0))
            ListView1.Items(读取计数).SubItems.Add(getdataa(账单读取, 1))
            ListView1.Items(读取计数).SubItems.Add(getdataa(账单读取, 2))
            ListView1.Items(读取计数).SubItems.Add(Val(getdataa(账单读取, 3)).ToString("0.00"))
            ListView1.Items(读取计数).SubItems.Add(getdataa(账单读取, 4))
            ListView1.Items(读取计数).SubItems.Add(Val(getdataa(账单读取, 5)).ToString("0.00"))
            当前总价 = 当前总价 + Val(getdataa(账单读取, 5))
            ListView1.Items(读取计数).SubItems.Add(getdataa(账单读取, 6))
            ListView1.Items(读取计数).SubItems.Add(getdataa(账单读取, 7))
            读取计数 = 读取计数 + 1
        Loop
        登录.连接.Close()
        Select Case 选取模式
            Case 0
                Label16.Text = "所有消费项目一共为$" & 当前总价.ToString("0.00")
            Case 1
                Label16.Text = "暂未付款项目一共为$" & 当前总价.ToString("0.00")
        End Select
    End Sub
    Private Sub 窗口获焦(sender As Object, e As EventArgs) Handles MyBase.Load, MyBase.MouseDown, GroupBox1.MouseDown, Button1.MouseDown, Button3.MouseDown, Button4.MouseDown
        If 身份证号缓存 = 主菜单.选中病人身份证号 Then
        Else
            身份证号刷新()
            身份证号缓存 = 主菜单.选中病人身份证号
        End If
        If Label9.Text <> "（无）" And 选取模式 = 1 Then
            'MsgBox("nb")
            Button4.Enabled = True
        Else
            'MsgBox("bnb")
            Button4.Enabled = False
        End If
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        选取病人.选取模式 = 0
        选取病人.更新需求 = True
        选取病人.Show()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        主菜单.Show()
        Me.Dispose()
    End Sub
    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        If Label9.Text <> "（无）" Then
            Dim 结账命令 As SqlCommand = New SqlCommand(
          "EXECUTE 结账 @身份证号码='" & 主菜单.选中病人身份证号 & "'")
            结账命令.Connection = 登录.连接
            If MsgBox("【" & Label7.Text & "】暂未付款项目一共为【$" & 当前总价.ToString("0.00") & "】您确认为其结账？", MsgBoxStyle.YesNo, "结账确认") = MsgBoxResult.Yes Then
                登录.连接.Open()
                If 结账命令.ExecuteNonQuery() > 0 Then
                    MsgBox("成功！")
                    登录.连接.Close()
                    Me.Dispose()
                Else
                    登录.连接.Close()
                End If
            End If
        Else
            MsgBox("没有正确地选取病人或治疗项目！",, "警告")
        End If
    End Sub
    Private Sub LinkLabel2_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel2.LinkClicked
        选取模式 = 1
        LinkLabel1.Enabled = True
        LinkLabel2.Enabled = False
        Button4.Enabled = True
        身份证号刷新()
    End Sub

    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        选取模式 = 0
        LinkLabel1.Enabled = False
        LinkLabel2.Enabled = True
        Button4.Enabled = False
        身份证号刷新()
    End Sub
End Class