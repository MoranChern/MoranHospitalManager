﻿Imports System.Data
Imports System.Data.SqlClient
Public Class 创建医嘱
    Public 选定治疗编号 As String = "000000"
    Dim 身份证号缓存 As String
    Dim 治疗编号缓存 As String
    Private Sub 身份证号刷新()
        Label7.Text = "（无）"
        Label8.Text = "（无）"
        Label9.Text = "（无）"
        Label10.Text = "（无）"
        Label12.Text = "（无）"
        Label14.Text = "（无）"
        TextBox1.Text = "（无）"
        TextBox2.Text = "（无）"
        Dim 病人选取命令 As SqlCommand = New SqlCommand(
"SELECT /*top (1000)*/
	  [姓名]
	  ,'在院' 状态
      ,[隐私化身份证号码]
      ,[性别]
      ,[年龄]
	  ,科室名称 现就诊科室
      --,[出生年月]
      ,[联系方式]
      ,[病人备注信息]
  FROM 在院病人信息
  where 身份证号码='" & 主菜单.选中病人身份证号 & "'
  Union All
  SELECT /*top (1000)*/
	  [姓名]
      ,'门诊' 状态
      ,[隐私化身份证号码]
      ,[性别]
      ,[年龄]
	  ,科室名称 现就诊科室
      --,[出生年月]
      ,[联系方式]
      ,[病人备注信息]
  FROM 今日门诊
  where 身份证号码='" & 主菜单.选中病人身份证号 & "'")
        Dim 选中病人信息读取 As System.Data.SqlClient.SqlDataReader '此行参考自学长的课题设计
        病人选取命令.Connection = 登录.连接
        登录.连接.Open()
        选中病人信息读取 = 病人选取命令.ExecuteReader
        Do While 选中病人信息读取.Read()
            Label7.Text = 选中病人信息读取.GetString(0)
            Label14.Text = 选中病人信息读取.GetString(1)
            Label8.Text = 选中病人信息读取.GetValue(2)
            Label10.Text = 选中病人信息读取.GetString(3)
            Label9.Text = 选中病人信息读取.GetValue(4)
            Label12.Text = 选中病人信息读取.GetString(5)
            TextBox1.Text = 选中病人信息读取.GetString(6)
            TextBox2.Text = 选中病人信息读取.GetString(7)
        Loop
        登录.连接.Close()
        If Label8.Text = "******************" Then
            Label8.Text = "（您不需要了解此字段）"
        End If
    End Sub
    Private Sub 治疗编号更新()
        Label16.Text = "暂未选择治疗"
        Dim 治疗名称抓取命令 As SqlCommand = New SqlCommand("
        select 治疗项目名称 from 治疗项目说明表 
        where 治疗项目编号='" & 选定治疗编号 & "' and Left(治疗项目编号,2)!='ZY'")
        治疗名称抓取命令.Connection = 登录.连接
        Dim 治疗名称读取 As System.Data.SqlClient.SqlDataReader
        登录.连接.Open()
        治疗名称读取 = 治疗名称抓取命令.ExecuteReader
        Do While 治疗名称读取.Read()
            Label16.Text = 治疗名称读取.GetString(0)
        Loop
        登录.连接.Close()
        Select Case Mid(选定治疗编号, 1, 2)
            Case "YP"
                Label17.Show()
                TextBox3.Text = ""
                TextBox3.Show()
            Case Else
                Label17.Hide()
                TextBox3.Text = "1"
                TextBox3.Hide()
        End Select
    End Sub
    Private Sub 窗口获焦(sender As Object, e As EventArgs) Handles MyBase.Load, MyBase.MouseDown, GroupBox1.MouseDown, TextBox3.MouseDown, Button1.MouseDown, Button2.MouseDown, Button3.MouseDown, Button4.MouseDown
        If 治疗编号缓存 = 选定治疗编号 Then
        Else
            治疗编号更新()
            治疗编号缓存 = 选定治疗编号
        End If
        If 身份证号缓存 = 主菜单.选中病人身份证号 Then
        Else
            身份证号刷新()
            身份证号缓存 = 主菜单.选中病人身份证号
        End If
        If Label9.Text <> "（无）" And IsNumeric(TextBox3.Text) And Label16.Text <> "暂未选择治疗" Then
            'MsgBox("nb")
            Button4.Enabled = True
        Else
            'MsgBox("bnb")
            Button4.Enabled = False
        End If
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        选取病人.选取模式 = 3
        选取病人.更新需求 = True
        选取病人.Show()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        主菜单.Show()
        Me.Dispose()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        选取治疗.选取模式 = 0
        选取治疗.更新需求 = True
        选取治疗.Show()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        If Label9.Text <> "（无）" And IsNumeric(TextBox3.Text) Then
            Dim 医嘱命令 As SqlCommand = New SqlCommand(
                       "EXECUTE 治疗事件 
                          @登记时间=null
                         ,@身份证号码='" & 主菜单.选中病人身份证号 & "'
                         ,@治疗项目编号='" & 选定治疗编号 & "'
                         ,@使用项目次数=" & TextBox3.Text & "
                         ,@治疗事件备注='" & InputBox("请输入备注信息（如果需要）", "备注信息输入") & "'")
            医嘱命令.Connection = 登录.连接
            Dim a As Boolean
            If Mid(选定治疗编号, 1, 2) = "YP" Then
                a = MsgBox("您确认为【" & Label14.Text & "】的【" & Label7.Text & "】开剂量为【" & TextBox3.Text & "】的【" & Label16.Text & "】吗？", MsgBoxStyle.YesNo, "转科信息确认") = MsgBoxResult.Yes
            Else
                a = MsgBox("您确认【" & Label14.Text & "】的【" & Label7.Text & "】需要进行【" & Label16.Text & "】吗？", MsgBoxStyle.YesNo, "转科信息确认") = MsgBoxResult.Yes
            End If
            If a Then
                登录.连接.Open()
                'MsgBox(TextBox3.Text)
                If 医嘱命令.ExecuteNonQuery() > 0 Then
                    MsgBox("成功！")
                End If
                登录.连接.Close()
                Me.Dispose()
            End If
        Else
            MsgBox("没有正确地选取病人或治疗项目！",, "警告")
        End If
    End Sub
End Class