﻿Public Class 病人信息管理
    'Dim Acceptnew As Boolean
    Private Sub 病人信息管理_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: 这行代码将数据加载到表“程氏医院管理系统数据库DataSet.病人信息表”中。您可以根据需要移动或删除它。
        Me.病人信息表TableAdapter.Fill(Me.程氏医院管理系统数据库DataSet.病人信息表)
        病人信息表BindingSource.AddNew()
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If 尝试保存() Then
            选取病人.更新需求 = True
            选取病人.Show()
            Me.Dispose()
        End If
    End Sub
    Private Function 尝试保存() As Integer
        Dim 身份证号码状态 As Boolean = False
        If Len(TextBox2.Text) = 18 Then
            'MsgBox（"不行吗？"）
            Dim i As Integer
            Dim k As Boolean = True
            For i = 1 To 18 Step 1
                If Mid(TextBox2.Text, i, 1) < "0" Or Mid(TextBox2.Text, i, 1) > "9" Then
                    If i = 18 And Mid(TextBox2.Text, i, 1) = "X" Then
                    Else
                        k = False
                    End If
                    ' MsgBox(i)
                End If
            Next i
            If k = True Then
                'MsgBox(Mid(TextBox2.Text, 7, 4))
                If Mid(TextBox2.Text, 7, 4) <= Year(Now) Then
                    ' MsgBox("冇问题啊？！")
                End If
                If Mid(TextBox2.Text, 7, 4) <= Year(Now) And Mid(TextBox2.Text, 11, 2) <= "12" And Mid(TextBox2.Text, 11, 2) <> "00" And Mid(TextBox2.Text, 13, 2) <> "00" Then
                    'Dim 所输入月字段 As Integer = Val(Mid(TextBox2.Text, 11, 2))
                    'Dim 所输入日字段 As Integer =
                    Select Case Mid(TextBox2.Text, 11, 2)
                        Case 1, 3, 5, 7, 8, 10, 12
                            If Mid(TextBox2.Text, 13, 2) > "31" Then
                                k = False
                            End If
                        Case 4, 6, 9, 11
                            If Mid(TextBox2.Text, 13, 2) > "30" Then
                                k = False
                            End If
                        Case 2
                            If Mid(TextBox2.Text, 7, 4) Mod 4 = 0 Then
                                If Mid(TextBox2.Text, 7, 4) Mod 100 = 0 Then
                                    If Mid(TextBox2.Text, 7, 4) Mod 400 = 0 Then
                                        If Mid(TextBox2.Text, 13, 2) > "29" Then
                                            k = False
                                        End If
                                    Else
                                        If Mid(TextBox2.Text, 13, 2) > "28" Then
                                            k = False
                                        End If
                                    End If
                                Else
                                    If Mid(TextBox2.Text, 13, 2) > "29" Then
                                        k = False
                                    End If
                                End If
                            Else
                                If Mid(TextBox2.Text, 13, 2) > "28" Then
                                    k = False
                                End If
                            End If
                    End Select
                    If k = True Then
                        身份证号码状态 = True
                    End If
                End If
            End If
        End If
        If 身份证号码状态 = False Or TextBox1.Text = "" Then
            If MsgBox("所作编辑无法保存，是否放弃更改？", MsgBoxStyle.YesNo, "拒绝保存警告") = MsgBoxResult.No Then
                Return False
            Else
                病人信息表BindingSource.CancelEdit()
            End If
        Else
            病人信息表BindingSource.EndEdit()
            病人信息表TableAdapter.Update(程氏医院管理系统数据库DataSet.病人信息表)
            'Acceptnew = True
            '病人信息表BindingSource.AddNew() '注意，这里已经是在建新
            主菜单.选中病人身份证号 = TextBox2.Text
        End If
        Return True
    End Function
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        尝试保存()
        病人信息表BindingSource.AddNew()
        'If Acceptnew Then
        '    Acceptnew = False
        '    病人信息表BindingSource.AddNew()
        '    Me.AcceptButton = Button2
        '    Button2.Text = "提交"
        'Else
        '    If 尝试保存() Then
        '        Me.AcceptButton = Nothing
        '        Button2.Text = "添加新记录"
        '    End If
        'End If
    End Sub
    '下面四个用来move
    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        If 尝试保存() Then
            病人信息表BindingSource.MoveFirst()
        End If
    End Sub
    Private Sub LinkLabel4_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel4.LinkClicked
        If 尝试保存() Then
            病人信息表BindingSource.MoveLast()
        End If
    End Sub
    Private Sub LinkLabel3_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel3.LinkClicked
        If 尝试保存() Then
            病人信息表BindingSource.MoveNext()
        End If
    End Sub
    Private Sub LinkLabel2_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel2.LinkClicked
        If 尝试保存() Then
            病人信息表BindingSource.MovePrevious()
        End If

    End Sub
    '下面三个是下行三个linktable
    Private Sub LinkLabel6_LinkClicked_1(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel6.LinkClicked
        病人信息表BindingSource.EndEdit()

    End Sub
    Private Sub LinkLabel7_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel7.LinkClicked
        病人信息表BindingSource.ResetBindings(True)
    End Sub
    Private Sub LinkLabel5_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel5.LinkClicked
        病人信息表BindingSource.AddNew()
    End Sub
End Class