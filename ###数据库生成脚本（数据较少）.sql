Use master
go
Create database
程氏医院管理系统数据库
/*on
Primary
(Name=数据库主文件,
filename=
 'D:\YuniC\OneDrive - yali.afsi.edu.au\Work\DB Project\天煞的课题设计\数据库主文件.mdf',
Maxsize=unlimited,
filegrowth=10%)
log on
(
name=数据库的日志,
filename=
 'D:\YuniC\OneDrive - yali.afsi.edu.au\Work\DB Project\天煞的课题设计\数据库的日志.ldf',
filegrowth=2)*/
go
use
程氏医院管理系统数据库 
go



Create table 系统用户表
	(用户名 VarChar(255) 
	,密码 text not null
	,全局管理权限 bit default 0 not null
	,门诊权限 bit default 0  not null
	,住院权限 bit default 0 not null
	,治疗权限 bit default 0 not null
	,Primary key(用户名)
	)
gO
Insert into 系统用户表(用户名,密码,全局管理权限,门诊权限,住院权限,治疗权限) Values
('Admin','123',1,1,1,1)
GO
Create table  病人信息表 
	(病人姓名 varchar(20) not null
	,病人身份证号 char(18) not null
	,病人联系方式 ntext null
	,病人备注信息 ntext not null default ''
	,primary key(病人身份证号 )
	)	 
go 
Insert into 病人信息表 Values
('Yuni Co','779834201902268902','手机：17722228789' ,'顶级厨师，程霂谷烹饪学博士' ),
('Yuni Co','779834201902268102','手机：1789'        ,'验证省份证号查重系统'       ),
('Mooc Co','779833201508198912','没得联系方式'      ,'程雨苓的下期食材！煎炸皆可' ),
('Kane De','000000200009250011',''                  ,'这是一个测试使用的病人'     ),
('程雨苓' ,'433234200006200881','18273777636'       ,'知名沙雕'                   ),
('Q某某'  ,'222222200001062222','不会接电话的'      ,'懒癌晚期，安宁养护为主'     ),
('程霂谷'              ,'349824200101250915','你可以发微信啊'    ,'知名食材'                   ),
('李·华为牛逼！！·孕','998876199802231111','大喊牛逼'          ,'两个字一杯酒'       ),
('这个名字就有点长了'  ,'643298200807091234','没有联系方式'      ,'不知道装不装得下啊'       ),
('牛逼！！'      ,'999999199909099999','不说牛逼'      ,'但还是牛逼好吧'       )
go



Create table 科室信息表 
	(科室编号 char(4)
	,科室名称 varchar(40) not null --这后面几列都是搞笑的就不上not null属性了
	,主治医生编号 char(10)
	,科室备注信息 ntext not null default '', primary key(科室编号 )
	) 
go
Insert into 科室信息表 Values
 ('0000','(无)',null,'这里是用来填写出院病人的')
,('CC01','肠胃科',null,' ')
,('CC02','传染科',null,' ')
,('CC03','胸外科',null,' ')
,('CC04','脑科'  ,null,' ')
,('CC05','内分泌科',null,' ')
,('CC06','妇产科',null,' ')
,('CC07','妇科',null,' ')
,('CC08','儿科',null,' ')
,('CC09','精神科',null,' ')
--,('CC99','食堂！',null,  '这个是来搞笑的' )
go


Create table 就诊事件表 
	(就诊类型 char(4) not null
	,病人身份证号 char(18) not null
	,科室编号 char(4) not null
	,系统录入时间 datetime
	,就诊事件备注 ntext not null default ''
	,事件自动编号 bigint identity(1,1)
	,primary key(事件自动编号 )
	,foreign key(病人身份证号 )references 病人信息表 (病人身份证号 )
	,foreign key(科室编号 )references 科室信息表 (科室编号)
	,check(就诊类型 in('门诊','入院','转科','出院')))
go
Insert into 就诊事件表 Values
	 ('门诊','779833201508198912','CC01','2019-04-30 16:13:15.927',' ')--CMG
	,('入院','779834201902268902','CC02','2019-04-22 16:13:15.927',' ')--CYL
	,('转科','779834201902268902','CC03','2019-04-23 16:13:15.927','这是一条写在转诊的备注')--CYL
	,('出院','779834201902268902','0000','2019-04-26 16:13:15.927',' ')--CYL
Go 
/*Create Trigger 就诊事件录入时间防止修改 On 就诊事件表 For Update As 
	IF(select 系统录入时间 from deleted)!=(select 系统录入时间 from inserted) 
		Begin 
			Raiserror('不可以编辑信息生成时间',16,1)
			RollBACK TRANSACTION 
		END
go
Create Trigger 自动录入时间 On 就诊事件表 For Insert As
	Update 就诊事件表 
		set 系统录入时间 =getdate() 
		where 事件自动编号 in (select 事件自动编号 from inserted)
GO*/
Create Table 治疗项目说明表
(
	治疗项目编号 char(6),--前两位字母是指的类型分为ZY（住院费用）CZ（操作，气压治疗，手术，检查等）YP（药品等）
	治疗项目名称 varchar(50) not null,
	[项目治疗费用] money not null,
	是否可叠加 bit not null,
	治疗项目备注与注意事项 ntext not null default '',
	Primary key(治疗项目编号)
)
go
Insert Into 治疗项目说明表 Values
 ('ZY0001','单日住院',$10.00,0,' ')
,('CZ0001','心电图',$15.58,0,' ')
,('CZ0002','器官移植手术',$17.28,0,' ')
,('CZ0003','小换药',$10.20,0,' ')
,('CZ0004','大换药',$30,0,' ')
,('CZ0005','多喝热水',$0.01,0,' ')
,('CZ0006','血常规',$12.5,0,' ')
,('CZ0007','尿检',$8.88,0,' ')
,('YP0001','阿司匹林0.2mg',$0.08,1,' ')
,('YP0002','开同1g',$0.2,1,' ')
,('YP0003','头孢0.2mg',$0.04,1,' ')
,('YP0004','人血白蛋白1瓶',$127.98,1,' ')
,('YP0005','生理盐水1袋',$0.05,1,' ')
,('YP0006','抗生素1瓶',$3,1,' ')
,('YP0007','葡萄糖1袋',$0.1,1,' ')
,('YP0008','胰岛素0.2mg',$20.8,1,' ')
,('YP0009','热水',$0.01,1,' ')
go
create function 治疗项目分类(@治疗项目编号 char(6))
returns Char(4)
As
Begin
If Left(@治疗项目编号,2)= 'ZY'
	Begin return '住院' END
Else Begin If Left(@治疗项目编号,2)= 'YP'
	Begin
	return '未知'
	End
	Else Begin If Left(@治疗项目编号,2)= 'CZ'
		Begin
		return '操作'
		End
		End
	end
return '未知'
end
go
Create Table 治疗事件表
(
	 登记时间 datetime null
	,病人身份证号 char(18) not null
	,治疗项目编号 char(6) not null 
	,使用项目次数 int null
	,是否付款 bit default 0 not null
	,治疗事件备注 ntext not null default ''
	,事件编号 bigint identity(1,1)
	,PRIMARY KEY CLUSTERED 
	([事件编号] DESC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
	,foreign key(病人身份证号)references 病人信息表(病人身份证号)
	,Foreign Key(治疗项目编号)References 治疗项目说明表(治疗项目编号)
)
GO
CREATE NONCLUSTERED INDEX [IX_治疗事件表] ON [dbo].[治疗事件表]
(
	[是否付款] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/*
Create Trigger 确保可叠加字段有效性 
	On 治疗事件表 
	For Update,insert AS
	If (select 使用项目次数 from inserted) is null and (select 是否可叠加 from 治疗项目说明表 where 治疗项目说明表.治疗项目编号=(select 治疗项目编号 from inserted))=1--是否可叠加=1
		Begin 
			Raiserror('“项目使用次数”字段应为空',16,1)
			RollBACK TRANSACTION 
		END
	If (select 使用项目次数 from inserted) is not null and (select 是否可叠加 from 治疗项目说明表 where 治疗项目说明表.治疗项目编号=(select 治疗项目编号 from inserted))=0
		Begin 
			Raiserror('“项目使用次数”字段不应为空',16,1)
			RollBACK TRANSACTION 
		END
go*/
/*下一步说明：这里需要的：
2.视图，所有未付款项目
3.视图单个病人所有记录（用于法律用途）
4.视图单个病人未付款
5.存储过程 付款
*/

  
--------------------------------------------------------------------------------------------
-- All Tables Created--
--------------------------------------------------------------------------------------------  
create function 隐私化身份证号函数(@姓名 varchar(20),@身份证号 Char(18))
returns Char(18)
As
Begin
If (select count(*)from 病人信息表 where 病人姓名=@姓名 and 病人身份证号=@身份证号)<>1
	return -1
--declare @输出 Char(18)='******************'
declare @循环计数 int = 1
While @循环计数<=18
	If (select count(*)from 病人信息表 where 病人姓名=@姓名 and 病人身份证号 LIKE Left(@身份证号,@循环计数)+'%')=1
		Break
	Else
	Begin
		if @循环计数=18
		Return @身份证号
		select @循环计数=@循环计数+1
		Continue
	End
return  Left(@身份证号,@循环计数-1)+Left('******************',19-@循环计数)
End
Go
create function 性别函数(@身份证号 Char(18))
returns Char(2)
As
Begin
If substring(@身份证号 ,17,1) in(1,3,5,7,9)
Return '男'
Return '女'
End
Go
Create view 病人信息 AS
	select
		 病人姓名 姓名
		,dbo.性别函数(病人身份证号) 性别
		,DATEDIFF(month,substring(病人身份证号 ,7,4)+'-'+substring(病人身份证号 ,11,2)+'-'+substring(病人身份证号 ,13,2),getdate())/12 年龄
		,substring(病人身份证号 ,7,4)+'年'+substring(病人身份证号 ,11,2)+'月' 出生年月
		,病人身份证号 身份证号码
		,dbo.隐私化身份证号函数(病人姓名,病人身份证号) 隐私化身份证号码
		,病人联系方式 联系方式
		,病人备注信息 病人备注信息 
		From 病人信息表
GO
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
Create view 在院病人信息 As
	Select 
	 科室名称 科室名称
	,姓名
	,性别
	,年龄
	,出生年月
	,病人信息.身份证号码 身份证号码
	,隐私化身份证号码
	,联系方式
	,病人备注信息
	,最近备注信息
		from 病人信息 Join 
		(
			select 事件编号,事件类型,身份证号码,科室编号,最近录入时间,最近备注信息
				from 
				(
					select
						事件自动编号 事件编号,
						就诊类型 事件类型,
						病人身份证号 身份证号码,
						科室编号 科室编号,
						系统录入时间 最近录入时间, 
						就诊事件备注 最近备注信息
					from 就诊事件表 A
					Where 事件自动编号= 
						(
							select top 1 事件自动编号
								from 就诊事件表 B 
								where 就诊类型!='门诊' and A.病人身份证号=B.病人身份证号
								Order by 事件自动编号 deSC
						)
				)筛去门诊病人和过期事件
				where 事件类型!='出院'
		) 在院病人事件 on 在院病人事件.身份证号码=病人信息.身份证号码 join 科室信息表 科室信息表 on 在院病人事件.科室编号=科室信息表.科室编号
go
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
Create view 今日门诊 As
	select 
	 科室名称 科室名称
	,姓名
	,性别
	,年龄
	,出生年月
	,病人信息.身份证号码 身份证号码
	,隐私化身份证号码
	,联系方式
	,病人备注信息
	,最近备注信息
		from 病人信息 Join
		(
			Select * from
				(
					select 
						事件自动编号 事件编号,
						就诊类型 事件类型,
						病人身份证号 身份证号码,
						科室编号 科室编号,
						系统录入时间 最近录入时间, 
						就诊事件备注 最近备注信息
						from 就诊事件表
						Where 就诊类型='门诊'
				)筛出门诊病人
				where CONVERT(VARCHAR(10),最近录入时间,110) =CONVERT(VARCHAR(10),GETDATE(),110)   and 身份证号码 not in(select 身份证号码 from 在院病人信息)
		)筛出今日门诊病人 on 筛出今日门诊病人.身份证号码=病人信息.身份证号码 join 科室信息表 科室信息表 on 筛出今日门诊病人.科室编号=科室信息表.科室编号
go
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
Create ProC 获取可入院门诊病人信息 @姓名 varchar(20) null,@身份证号码 varchar(18) null as 
SELECT /*top (1000)*/
	  [姓名]
      ,[身份证号码]
      ,[隐私化身份证号码]
      ,[性别]
      ,[年龄]
	  ,'（无）' 现就诊科室
      --,[出生年月]
      ,[联系方式]
      ,[病人备注信息]
  FROM 病人信息
  where [姓名] like '%'+@姓名+'%' and [身份证号码] like '%'+@身份证号码+'%'
  and 身份证号码 not in
  (select 身份证号码 from 在院病人信息
  union
  select 身份证号码 from 今日门诊)
go
Create ProC 获取在院病人信息 @姓名 varchar(20) null,@身份证号码 varchar(18) null as 
SELECT /*top (1000)*/
	  [姓名]
      ,[身份证号码]
      ,[隐私化身份证号码]
      ,[性别]
      ,[年龄]
	  ,科室名称 现就诊科室
      --,[出生年月]
      ,[联系方式]
      ,[病人备注信息]
  FROM 在院病人信息
  where [姓名] like '%'+@姓名+'%' and [身份证号码] like '%'+@身份证号码+'%'
go
Create ProC 获取可治疗病人信息 @姓名 varchar(20) null,@身份证号码 varchar(18) null as 
SELECT /*top (1000)*/
	  [姓名]
      ,[身份证号码]
      ,[隐私化身份证号码]
      ,[性别]
      ,[年龄]
	  ,科室名称 现就诊科室
      --,[出生年月]
      ,[联系方式]
      ,[病人备注信息]
  FROM 在院病人信息
  where [姓名] like '%'+@姓名+'%' and [身份证号码] like '%'+@身份证号码+'%'
  Union All
  SELECT /*top (1000)*/
	  [姓名]
      ,[身份证号码]
      ,[隐私化身份证号码]
      ,[性别]
      ,[年龄]
	  ,科室名称 现就诊科室
      --,[出生年月]
      ,[联系方式]
      ,[病人备注信息]
  FROM 今日门诊
  where [姓名] like '%'+@姓名+'%' and [身份证号码] like '%'+@身份证号码+'%'
go

Create ProC 获取全局病人信息 @姓名 varchar(20) null,@身份证号码 varchar(18) null as 
SELECT /*top (1000)*/
	  [姓名]
      ,[身份证号码]
      ,[隐私化身份证号码]
      ,[性别]
      ,[年龄]
	  ,科室名称 现就诊科室
      --,[出生年月]
      ,[联系方式]
      ,[病人备注信息]
  FROM 在院病人信息
  where [姓名] like '%'+@姓名+'%' and [身份证号码] like '%'+@身份证号码+'%'
  Union All
  SELECT /*top (1000)*/
	  [姓名]
      ,[身份证号码]
      ,[隐私化身份证号码]
      ,[性别]
      ,[年龄]
	  ,科室名称 现就诊科室
      --,[出生年月]
      ,[联系方式]
      ,[病人备注信息]
  FROM 今日门诊
  where [姓名] like '%'+@姓名+'%' and [身份证号码] like '%'+@身份证号码+'%'
  Union all
  SELECT /*top (1000)*/
	  [姓名]
      ,[身份证号码]
      ,[隐私化身份证号码]
      ,[性别]
      ,[年龄]
	  ,'（无）' 现就诊科室
      --,[出生年月]
      ,[联系方式]
      ,[病人备注信息]
  FROM 病人信息
  where [姓名] like '%'+@姓名+'%' and [身份证号码] like '%'+@身份证号码+'%'
  and 身份证号码 not in
  (select 身份证号码 from 在院病人信息
  union
  select 身份证号码 from 今日门诊)
go

create function 付款状态函数(@付款状态 bit)
returns char(6)
As
Begin
If @付款状态=0
Return '未付款'
Return '已付款'
End
go
Create View 所有治疗项目 AS
	Select 
		 登记时间
		,病人姓名 姓名
		,治疗项目名称
		,null 份数
		,项目治疗费用 费用
		,dbo.付款状态函数(是否付款) 付款状态
		,'病人身份证号：'+治疗事件表.病人身份证号+'病人联系方式（可能为空）：'+CONVERT(varchar,病人联系方式)+'。治疗事件备注（可能为空）：'+CONVERT(varchar,治疗事件备注) 备注
		from  
			病人信息表 
			Join 治疗事件表
			On 病人信息表.病人身份证号=治疗事件表.病人身份证号 
			Join 治疗项目说明表 
			On 治疗项目说明表.治疗项目编号=治疗事件表.治疗项目编号 
		Where 是否可叠加=0
	Union
	Select
		 登记时间
		,病人姓名 姓名
		,治疗项目名称,使用项目次数 份数
		,项目治疗费用*使用项目次数 费用
		,dbo.付款状态函数(是否付款) 付款状态
		,'病人身份证号：'+治疗事件表.病人身份证号+'病人联系方式（可能为空）：'+CONVERT(varchar,病人联系方式)+'。治疗事件备注（可能为空）：'+CONVERT(varchar,治疗事件备注) 备注
		from  
			病人信息表 
			Join 治疗事件表
			On 病人信息表.病人身份证号=治疗事件表.病人身份证号 
			Join 治疗项目说明表 
			On 治疗项目说明表.治疗项目编号=治疗事件表.治疗项目编号 
		Where 是否可叠加=1
go
CreATe pRoc 列举账单 @身份证号码 char(18) ,@选取模式 int
as
Begin
If @选取模式=0 begin
	Select 
         CONVERT(varchar,事件编号) 编号 
		,CONVERT(varchar,登记时间) 登记时间
	--	,病人姓名 姓名
		,CONVERT(varchar,治疗项目名称) 治疗项目名称
		,null 单价
		,null 份数
		,CONVERT(varchar,项目治疗费用) 单项费用
		,CONVERT(varchar,dbo.付款状态函数(是否付款)) 付款状态
		,CONVERT(varchar,治疗事件备注) 备注
		from  
			治疗事件表
			Join 治疗项目说明表 
			On 治疗项目说明表.治疗项目编号=治疗事件表.治疗项目编号 
		Where 病人身份证号 =@身份证号码 and 是否可叠加=0
	Union
	Select
         事件编号 编号 
		,登记时间
	--	,病人姓名 姓名
		,CONVERT(varchar,治疗项目名称)
		,CONVERT(varchar,项目治疗费用) 单价
		,CONVERT(varchar,使用项目次数) 份数
		,CONVERT(varchar,项目治疗费用*使用项目次数) 单项费用
		,CONVERT(varchar,dbo.付款状态函数(是否付款)) 付款状态
		,CONVERT(varchar,治疗事件备注) 备注
		from  
			治疗事件表 
			Join 治疗项目说明表 
			On 治疗项目说明表.治疗项目编号=治疗事件表.治疗项目编号 
		Where 病人身份证号 =@身份证号码 and 是否可叠加=1
end
If @选取模式=1 begin
	Select 
         事件编号 编号 
		,登记时间
	--	,病人姓名 姓名
		,治疗项目名称
		,null 单价
		,null 份数
		,项目治疗费用 单项费用
		,'未付款' 付款状态
		,CONVERT(varchar,治疗事件备注) 备注
		from  
			治疗事件表
			Join 治疗项目说明表 
			On 治疗项目说明表.治疗项目编号=治疗事件表.治疗项目编号 
		Where 是否付款 =0 and 病人身份证号 =@身份证号码 and 是否可叠加=0
	Union
	Select
         事件编号 编号 
		,登记时间
	--	,病人姓名 姓名
		,治疗项目名称
		,项目治疗费用 单价
		,使用项目次数 份数
		,项目治疗费用*使用项目次数 单项费用
		,'未付款' 付款状态
		,CONVERT(varchar,治疗事件备注) 备注
		from  
			治疗事件表 
			Join 治疗项目说明表 
			On 治疗项目说明表.治疗项目编号=治疗事件表.治疗项目编号 
		Where 是否付款 =0 and 病人身份证号 =@身份证号码 and 是否可叠加=1
end
End
go
CReatE prOc 结账 @身份证号码 char(18) as
begin
update 治疗事件表
set 是否付款 =1
where 病人身份证号 =@身份证号码 and 是否付款 =0
end
go
/*
Create Table 治疗项目说明表
(
	治疗项目编号 char(6),
	治疗项目名称 varchar(50) not null,
	项目治疗费用 money not null,
	是否可叠加 bit not null,
	治疗项目备注与注意事项 ntext null,
	Primary key(治疗项目编号)
Create Table 治疗事件表
(
	登记时间 datetime null,
	病人身份证号 char(18) not null,
	治疗项目编号 char(6) not null,
	使用项目次数 int null,
	是否付款 bit not null,
	治疗事件备注 ntext,
	事件编号 bigint identity(1,1),
/*下一步说明：这里需要的：
2.视图，所有未付款项目
3.视图单个病人所有记录（用于法律用途）
4.视图单个病人未付款
5.存储过程 付款
*/*/

--------------------------------------------------------------------------------------------
-- All Views Created--
-------------------------------------------------------------------------------------------- 

Create Proc 本次在院最近一次备注 @身份证号码 char(18), @备注 ntext OUTput as
begin
declare @最近一次入院 bigint = (select top 1 事件自动编号 from 就诊事件表 where 就诊类型 ='入院')
declare @最近一次出院 bigint = (select top 1 事件自动编号 from 就诊事件表 where 就诊类型 ='出院')
If @最近一次入院 is not null and (@最近一次入院>@最近一次出院 or @最近一次出院 is null)
Begin
select @备注=就诊事件备注
from 就诊事件表
			where 病人身份证号=@身份证号码 and 就诊类型 <>'门诊' and 事件自动编号>@最近一次入院
			Order by 事件自动编号 DESC
end
	end
go
/*
事件自动编号 bigint identity(1,1),
就诊类型 char(4) not null, --这里来个外键约束
病人身份证号 char(18) not null,--难受受这里有外键约束啊！！！！！
科室编号 char(4) not null,/*--啊啊啊啊这里也要写外键约束啊！！
特殊症状编号四位前两位是字母啊啊啊啊啊啊啊啊啊 char(4) null,*/
系统录入时间 datetime /*default(getdate())*/, 
就诊事件备注*/

Create ProC 门诊挂号 @身份证号码 char(18) ,@备注 ntext,@科室 char(4),@系统录入时间 datetime  as
	If @系统录入时间 is null
	Begin 
		Insert into 就诊事件表 values
			('门诊',@身份证号码,@科室,getdate(),@备注)
	End
	Else Begin
		Insert into 就诊事件表 values
			('门诊',@身份证号码,@科室,@系统录入时间,@备注)
	End
go

Create proC 病人入院 @身份证号码 char(18) ,@备注 ntext,@科室 char(4),@系统录入时间 datetime  as
If @系统录入时间 is null
	Begin 
		Insert into 就诊事件表 values
			('入院',@身份证号码,@科室,getdate(),@备注)
	End
	Else Begin
		Insert into 就诊事件表 values
			('入院',@身份证号码,@科室,@系统录入时间,@备注)
	End
		/*Insert into 就诊事件表 values
			('入院',@身份证号码,@科室,null,@备注)*/
go

Create proC 住院病人科室转换 @身份证号码 char(18) ,@备注 ntext,@科室 char(4),@系统录入时间 datetime  as
	If @备注 is null begin Exec [本次在院最近一次备注] @身份证号码,@备注 OUTPUT end
	If @系统录入时间 is null
	Begin 
		Insert into 就诊事件表 values
			('转科',@身份证号码,@科室,getdate(),@备注)
	End
	Else Begin
		Insert into 就诊事件表 values
			('转科',@身份证号码,@科室,@系统录入时间,@备注)
	End
go---在这里可以用[在院病人信息]枚举出可供转科的病人

Create proC 出院  @身份证号码 char(18) ,@备注 ntext,@系统录入时间 datetime as
	If @备注 is null begin Exec [本次在院最近一次备注] @身份证号码,@备注 OUTPUT end
	If @系统录入时间 is null
	Begin 
		Insert into 就诊事件表 values
			('出院',@身份证号码,'0000',getdate(),@备注)
	End
	Else Begin
		Insert into 就诊事件表 values
			('出院',@身份证号码,'0000',@系统录入时间,@备注)
	End
go---在这里可以用[在院病人信息]枚举出可出院的病人

Create proC 治疗事件  @登记时间 datetime,@身份证号码 char(18) ,@治疗项目编号 char(6),@使用项目次数 int,@治疗事件备注 ntext as
If  @登记时间 is null
Begin
Insert 治疗事件表(登记时间,病人身份证号,治疗项目编号,使用项目次数,治疗事件备注) Values
(getdate(),@身份证号码,@治疗项目编号,@使用项目次数,@治疗事件备注)
End
Else Begin
Insert 治疗事件表(登记时间,病人身份证号,治疗项目编号,使用项目次数,治疗事件备注) Values
(@登记时间,@身份证号码,@治疗项目编号,@使用项目次数,@治疗事件备注)
End
Go
Use Master
go

