﻿Imports System.Data
Imports System.Data.SqlClient
Public Class 入院登记
    Dim 身份证号缓存 As String
    Private Sub 刷新()
        Label7.Text = "（无）"
        Label8.Text = "（无）"
        Label9.Text = "（无）"
        Label10.Text = "（无）"
        TextBox1.Text = "（无）"
        TextBox2.Text = "（无）"
        Dim 病人选取命令 As SqlCommand = New SqlCommand(
       "If " & 主菜单.选中病人身份证号 & " not in  
       (select 身份证号码 from 在院病人信息
        union
        select 身份证号码 from 今日门诊)
       Begin         
       Select 
             姓名
            ,隐私化身份证号码
            ,年龄
            ,性别
            ,联系方式
            ,病人备注信息
            from 病人信息 
            where 身份证号码='" & 主菜单.选中病人身份证号 & "'
       End ")
        Dim 选中病人信息读取 As System.Data.SqlClient.SqlDataReader '此行参考自学长的课题设计
        病人选取命令.Connection = 登录.连接
        登录.连接.Open()
        'Label7.Text = 病人选取命令.ExecuteReader
        选中病人信息读取 = 病人选取命令.ExecuteReader
        Do While 选中病人信息读取.Read()
            Label7.Text = 选中病人信息读取.GetString(0)
            Label8.Text = 选中病人信息读取.GetString(1)
            Label9.Text = 选中病人信息读取.GetValue(2)
            Label10.Text = 选中病人信息读取.GetString(3)
            TextBox1.Text = 选中病人信息读取.GetString(4)
            TextBox2.Text = 选中病人信息读取.GetString(5)
        Loop
        If Label8.Text = "******************" Then
            Label8.Text = "（您不需要了解此字段）"
        End If
        登录.连接.Close()
        If Label9.Text = "（无）" Then
            'MsgBox（“恶魔”）
            Button1.Enabled = False
            Button2.Enabled = False
        Else
            Button1.Enabled = True
            Button2.Enabled = True
        End If
    End Sub
    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        主菜单.Show()
        Me.Dispose()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        选取病人.选取模式 = 1
        选取病人.更新需求 = True
        选取病人.Show()
    End Sub

    Private Sub 入院登记_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: 这行代码将数据加载到表“程氏医院管理系统数据库DataSet.科室信息表”中。您可以根据需要移动或删除它。
        Me.科室信息表TableAdapter.Fill(Me.程氏医院管理系统数据库DataSet.科室信息表)
        刷新()
        身份证号缓存 = 主菜单.选中病人身份证号
    End Sub

    Private Sub 入院登记_MouseDown(sender As Object, e As MouseEventArgs) Handles Me.MouseDown, Button1.MouseDown, Button2.MouseDown, Button3.MouseDown, Button4.MouseDown, ComboBox1.MouseDown
        'MsgBox（"牛逼！"）
        If 身份证号缓存 = 主菜单.选中病人身份证号 Then
        Else
            'MsgBox（"牛逼！!"）
            刷新()
            身份证号缓存 = 主菜单.选中病人身份证号
        End If
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If Label9.Text = "（无）" Then
            MsgBox("没有正确地选取病人！",, "警告")
        Else
            If ComboBox1.SelectedIndex > 0 Then
                Dim 住院命令 As SqlCommand = New SqlCommand(
                   "EXECUTE /*@RC = [dbo].*/[病人入院] 
   @身份证号码='" & 主菜单.选中病人身份证号 & "'
  ,@备注='" & InputBox("请输入备注信息（如果需要）", "备注信息输入") & "' --这一行在书的P253
  ,@科室='" & ComboBox1.SelectedValue & "'
  ,@系统录入时间=null")
                住院命令.Connection = 登录.连接
                If MsgBox("您确认吗将【" & Label7.Text & "】安排在【" & ComboBox1.Text & "】进行【住院】吗？", MsgBoxStyle.YesNo, "入院信息确认") = MsgBoxResult.Yes Then
                    登录.连接.Open()
                    If 住院命令.ExecuteNonQuery() > 0 Then
                        MsgBox("成功！")
                    End If
                    登录.连接.Close()
                    Me.Dispose()
                End If
            Else
                MsgBox("没有正确地选择科室！",, "警告")
            End If
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If Label9.Text = "（无）" Then
            MsgBox("没有正确地选取病人！",, "警告")
        Else
            If ComboBox1.SelectedIndex > 0 Then
                'MsgBox("这是" & ComboBox1.Text & " - " & ComboBox1.SelectedIndex & " - " & ComboBox1.SelectedValue)
                Dim 门诊命令 As SqlCommand = New SqlCommand( 'inputbox在书的P253
"EXECUTE /*@RC = [dbo].*/[门诊挂号] 
   @身份证号码='" & 主菜单.选中病人身份证号 & "'
  ,@备注='" & InputBox("请输入备注信息（如果需要）", "备注信息输入") & "'
  ,@科室='" & ComboBox1.SelectedValue & "'
  ,@系统录入时间=null")
                门诊命令.Connection = 登录.连接
                If MsgBox("您确认吗将【" & Label7.Text & "】安排在【" & ComboBox1.Text & "】进行【门诊就诊】吗？", MsgBoxStyle.YesNo, "门诊就诊信息确认") = MsgBoxResult.Yes Then
                    登录.连接.Open()
                    If 门诊命令.ExecuteNonQuery() > 0 Then
                        MsgBox("成功！")
                    End If
                    登录.连接.Close()
                    Me.Dispose()
                End If
            Else
                MsgBox("没有正确地选择科室！",, "警告")
            End If
        End If
    End Sub
End Class