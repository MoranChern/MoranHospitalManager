﻿Imports System.Data
Imports System.Data.SqlClient
Public Class 病人信息编辑
    Dim 病人读取命令 As SqlCommand
    Dim 病人信息读取 As System.Data.SqlClient.SqlDataReader
    Public 更新需求 As Boolean
    Private Sub 触发查询()
        病人读取命令 = New SqlCommand(
            "SELECT /*top (1000)*/
	  [病人姓名]
     ,[病人联系方式]
     ,[病人备注信息]
     ,dbo.隐私化身份证号函数(病人姓名,'" & 主菜单.选中病人身份证号 & "') 隐私化身份证号码
  FROM [程氏医院管理系统数据库].[dbo].[病人信息表]
  where [病人身份证号] = '" & 主菜单.选中病人身份证号 & "'")
        病人读取命令.Connection = 登录.连接
        登录.连接.Open()
        病人信息读取 = 病人读取命令.ExecuteReader
        Do While 病人信息读取.Read()
            TextBox1.Text = 病人信息读取.GetValue(0)
            TextBox2.Text = 病人信息读取.GetValue(1)
            TextBox3.Text = 病人信息读取.GetValue(2)
            TextBox4.Text = 病人信息读取.GetValue(3)

        Loop
        登录.连接.Close()
    End Sub
    Private Function 尝试保存() As Integer
        Dim 身份证号码状态 As Boolean = True
        'If Len(TextBox2.Text) = 18 Then
        '    'MsgBox（"不行吗？"）
        '    Dim i As Integer
        '    Dim k As Boolean = True
        '    For i = 1 To 18 Step 1
        '        If Mid(TextBox2.Text, i, 1) < "0" Or Mid(TextBox2.Text, i, 1) > "9" Then
        '            If i = 18 And Mid(TextBox2.Text, i, 1) = "X" Then
        '            Else
        '                k = False
        '            End If
        '            ' MsgBox(i)
        '        End If
        '    Next i
        '    If k = True Then
        '        'MsgBox(Mid(TextBox2.Text, 7, 4))
        '        If Mid(TextBox2.Text, 7, 4) <= Year(Now) Then
        '            ' MsgBox("冇问题啊？！")
        '        End If
        '        If Mid(TextBox2.Text, 7, 4) <= Year(Now) And Mid(TextBox2.Text, 11, 2) <= "12" And Mid(TextBox2.Text, 11, 2) <> "00" And Mid(TextBox2.Text, 13, 2) <> "00" Then
        '            'Dim 所输入月字段 As Integer = Val(Mid(TextBox2.Text, 11, 2))
        '            'Dim 所输入日字段 As Integer =
        '            Select Case Mid(TextBox2.Text, 11, 2)
        '                Case 1, 3, 5, 7, 8, 10, 12
        '                    If Mid(TextBox2.Text, 13, 2) > "31" Then
        '                        k = False
        '                    End If
        '                Case 4, 6, 9, 11
        '                    If Mid(TextBox2.Text, 13, 2) > "30" Then
        '                        k = False
        '                    End If
        '                Case 2
        '                    If Mid(TextBox2.Text, 7, 4) Mod 4 = 0 Then
        '                        If Mid(TextBox2.Text, 7, 4) Mod 100 = 0 Then
        '                            If Mid(TextBox2.Text, 7, 4) Mod 400 = 0 Then
        '                                If Mid(TextBox2.Text, 13, 2) > "29" Then
        '                                    k = False
        '                                End If
        '                            Else
        '                                If Mid(TextBox2.Text, 13, 2) > "28" Then
        '                                    k = False
        '                                End If
        '                            End If
        '                        Else
        '                            If Mid(TextBox2.Text, 13, 2) > "29" Then
        '                                k = False
        '                            End If
        '                        End If
        '                    Else
        '                        If Mid(TextBox2.Text, 13, 2) > "28" Then
        '                            k = False
        '                        End If
        '                    End If
        '            End Select
        '            If k = True Then
        '                身份证号码状态 = True
        '            End If
        '        End If
        '    End If
        'End If
        If 身份证号码状态 = False Or TextBox1.Text = "" Then
            If MsgBox("所作编辑无法保存，是否放弃更改？", MsgBoxStyle.YesNo, "拒绝保存警告") = MsgBoxResult.No Then
                Return False
            Else
                '病人信息表BindingSource.CancelEdit()
            End If
        Else
            病人读取命令 = New SqlCommand(
            "UPDATE [dbo].[病人信息表]
   SET /*[病人姓名] ='" & TextBox1.Text & "'
      --,[病人身份证号] = 
      ,*/[病人联系方式] ='" & TextBox2.Text & "'
      ,[病人备注信息] ='" & TextBox3.Text & "'
  where [病人身份证号] = '" & 主菜单.选中病人身份证号 & "'")
            病人读取命令.Connection = 登录.连接
            登录.连接.Open()
            病人读取命令.ExecuteNonQuery()
            登录.连接.Close()
            选取病人.更新需求 = True
        End If
        Return True
    End Function
    Private Sub 病人信息编辑_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        触发查询()
    End Sub
    Private Sub 对焦(sender As Object, e As EventArgs) Handles Me.MouseDown, TextBox1.MouseDown, TextBox2.MouseDown, TextBox3.MouseDown
        If 更新需求 Then
            触发查询()
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Dispose()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        尝试保存()
        选取病人.更新需求 = True
        Me.Dispose()
    End Sub
End Class